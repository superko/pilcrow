# Pilcrow

Pilcrow is minimal text editor for writers. It allows you to:

- write and reorder multiple "pages"
- save text in "variables" which can be renamed at will, so you can (for example) begin writing with a placeholder character name and then replace it later

That's it! It is deliberately feature-light. It was designed to solve exactly one problem for fiction authors.

## Please note

Pilcrow is in its very early stages. It is highly recommended that you save regularly as you work, and make regular backups of your `.pilcrow` files.

Pilcrow is not intended to fully replace conventional word processors in your process. It's specifically meant for the early stages of writing fictional works, in which names of people/places/objects are often in flux. Once you've begun editing/formatting, you should export your raw text and leave Pilcrow behind.

## Known bugs

- link decorator bug
