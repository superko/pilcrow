# TODO

## Features

core

- [x] load files
- [x] save files
- [x] insert variables
- [ ] raw text export
  - [ ] individual page
  - [ ] entire project

extra

- [x] multiple pages in a project
- [ ] auto open recent file
- [ ] save settings (to file?)
- [ ] delete pages
- [ ] delete variables?
- [ ] user-adjustable font & margins
- [ ] drag sidebar width

a11y

- [ ] keyboard-nav detection
- [ ] keyboard shortcuts in general

bugs

- [ ] when cursor is immediately before or after a link decorator, only space key works, need to manually click to reset cursor

polish

- [x] maintain caret position after variable insert
- [ ] transition animations
- [ ] loading screen
- [ ] logos & icons
- [x] dirty/save feedback
- [ ] confirmation dialog for closing page/project/file without saving

tech

- [x] eject create-react-app, greatly reduce build complexity and deps
- [ ] use `@emotion`'s `css` prop in place of `className`
- [ ] rewrite contextBridge functions to async
- [ ] error handling
- [x] verify raw text accessible in contentState save data
- [x] deal with `string | number` types
- [ ] remove `rawText` from `FileState.pages`, only convert to raw when needed (so variable updates proliferate)

web version

- [ ] build for browser
- [ ] optional save to localstorage
- [ ] user accounts
