const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('electronAPI', {
  showSaveDialog: (args) => ipcRenderer.sendSync('show-save-dialog', args),
  showOpenDialog: (args) => ipcRenderer.sendSync('show-open-dialog', args),
  writeFile: (args) => ipcRenderer.sendSync('write-file', args),
  loadFile: (args) => ipcRenderer.sendSync('load-file', args),
});
