import { css } from '@emotion/css';

import { AppContextProvider } from './state/AppContextProvider';
import { Menu } from './components/Menu';
import { EmptyStateDialog } from './components/EmptyStateDialog';
import { MainPanel } from './components/MainPanel';
import { Notifications } from './components/Notifications';
import { injectGlobalStyles } from './styles/global';

injectGlobalStyles();

function App() {
  return (
    <AppContextProvider>
      <div
        className={css`
          display: flex;
          width: 100%;
          max-width: 100%;
          height: 100%;
        `}
      >
        <MainPanel />
        <Menu />
        <Notifications />
        <EmptyStateDialog />
      </div>
    </AppContextProvider>
  );
}

export default App;
