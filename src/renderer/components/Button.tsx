import React from 'react';
import { css } from '@emotion/css';
import { ConditionalWrapper } from 'renderer/components/ConditionalWrapper';

interface Props {
  children: React.ReactNode;
  onClick?: (ev: React.MouseEvent<HTMLButtonElement>) => void;
  onMouseEnter?: (ev: React.MouseEvent<HTMLLabelElement>) => void;
  onMouseLeave?: (ev: React.MouseEvent<HTMLLabelElement>) => void;
  onMouseDown?: (ev: React.MouseEvent<HTMLLabelElement>) => void;
  onMouseUp?: (ev: React.MouseEvent<HTMLLabelElement>) => void;
  href?: string;
  title?: string;
  disabled?: boolean;
  opaque?: boolean;
  className?: string;
}

export function Button({
  children,
  onClick,
  onMouseEnter,
  onMouseLeave,
  onMouseDown,
  onMouseUp,
  href,
  title,
  disabled,
  opaque,
  className,
}: Props) {
  const disabledStyling = disabled
    ? `
    color: grey;
    text-decoration: line-through;
  `
    : `
    &:hover {
      color: grey;
    }
  `;

  const opaqueStyling = opaque
    ? `
    background-color: white;
  `
    : '';

  return (
    <label
      className={
        `${className ? `${className} ` : ''}` +
        css`
          padding: 0.4rem 0.2rem;
          border: 1px ${disabled ? 'dotted' : 'solid'};
          user-select: none;
          text-transform: uppercase;
          ${disabledStyling}
          ${opaqueStyling}
        `
      }
      {...{ title, onMouseEnter, onMouseLeave, onMouseDown, onMouseUp }}
    >
      <ConditionalWrapper
        condition={Boolean(onClick)}
        wrapper={(c) => (
          <button
            type="button"
            className={css`
              ${disabledStyling}
              ${opaqueStyling}
              text-transform: uppercase;
            `}
            onClick={onClick}
            disabled={disabled}
          >
            {c}
          </button>
        )}
      >
        <ConditionalWrapper
          condition={Boolean(href)}
          wrapper={(c) => (
            <a
              href={href}
              className={css`
                ${disabledStyling}
                ${disabled ? 'pointer-events: none;' : ''}
              `}
            >
              {c}
            </a>
          )}
        >
          {children}
        </ConditionalWrapper>
      </ConditionalWrapper>
    </label>
  );
}
