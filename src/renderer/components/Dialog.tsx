import React from 'react';
import { css } from '@emotion/css';
import { Button } from './Button';

interface Props {
  title?: string;
  children?: React.ReactNode;
  isVisible?: boolean;
  onClose?: () => void;
  actions?: {
    text: string;
    onClick: () => void;
    disabled?: boolean;
  }[];
}

export function Dialog({
  title,
  children,
  isVisible,
  onClose,
  actions,
}: Props) {
  if (!isVisible) return null;
  return (
    <dialog
      className={css`
        position: fixed;
        z-index: 998;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        border: 0;
        background-color: rgba(0, 0, 0, 0.5);
      `}
    >
      <div
        className={css`
          width: 50%;
          min-width: 300px;
          max-height: 80%;
          background-color: white;
          display: flex;
          flex-direction: column;
        `}
      >
        <div
          className={css`
            padding: 0.5rem 1rem;
            border-bottom: 1px solid;
            background-color: black;
            color: white;
            text-transform: uppercase;
            position: relative;
            display: flex;
          `}
        >
          {title ? <div>{title}</div> : null}
          {onClose ? (
            <button
              type="button"
              onClick={onClose}
              className={css`
                position: absolute;
                right: 0;
                color: inherit;
              `}
            >
              ×
            </button>
          ) : null}
        </div>
        {children ? (
          <div
            className={css`
              padding: 1rem;
            `}
          >
            {children}
          </div>
        ) : null}
        {actions?.length ? (
          <div
            className={css`
              padding: 2rem 1rem;
              text-align: right;
            `}
          >
            {actions.map((action) => (
              <Button key={action.text} onClick={action.onClick}>
                {action.text}
              </Button>
            ))}
          </div>
        ) : null}
      </div>
    </dialog>
  );
}
