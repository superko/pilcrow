import React from 'react';
import { AppContext } from 'renderer/state';
import { createNewFile, loadFile, showOpenDialog } from 'renderer/utils';
import { Dialog } from './Dialog';

export function EmptyStateDialog() {
  const { selectedPage, setSelectedFilePath, fileState, setFileState } =
    React.useContext(AppContext);

  const loadFileToState = (filepath: string) => {
    const file = loadFile(filepath);
    setFileState(file);
    setSelectedFilePath(filepath);
  };

  const handleCreateNew = () => {
    const filepath = createNewFile();
    if (filepath) {
      loadFileToState(filepath);
    }
  };

  const handleOpenExisting = () => {
    const filepath = showOpenDialog();
    if (!filepath) return;
    loadFileToState(filepath);
  };

  if (fileState || selectedPage) return null;

  return (
    <Dialog
      isVisible
      title="A blank page"
      actions={[
        {
          text: 'Create new',
          onClick: handleCreateNew,
        },
        {
          text: 'Open existing',
          onClick: handleOpenExisting,
        },
      ]}
    >
      There&apos;s no Pilcrow project selected. What would you like to do?
    </Dialog>
  );
}
