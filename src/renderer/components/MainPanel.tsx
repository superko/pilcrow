import React from 'react';
import { AppContext } from 'renderer/state';
import { PageSelector } from './PageSelector';
import { TextEditor } from './TextEditor';

export function MainPanel() {
  const { fileState, selectedPage } = React.useContext(AppContext);

  if (fileState === null) return null;
  if (selectedPage === null) {
    return <PageSelector />;
  }
  return <TextEditor />;
}
