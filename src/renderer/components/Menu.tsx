import React from 'react';
import { css } from '@emotion/css';
import { AppContext } from 'renderer/state';
import { Button } from './Button';
import { VariablesEditor } from './VariablesEditor';

export function Menu() {
  const [isMenuOpen, setIsMenuOpen] = React.useState(true);

  const {
    fileState,
    saveExistingFile,
    selectedPage,
    setSelectedPage,
    setFileState,
  } = React.useContext(AppContext);
  const isMenuEnabledAndOpen = isMenuOpen && !!fileState;

  return (
    <menu
      className={css`
        display: flex;
        height: 100%;
        z-index: 100;
        top: 0;
        right: 0;
      `}
    >
      <button
        type="button"
        className={css`
          position: fixed;
          top: 1rem;
          right: 1rem;
          display: flex;
          align-items: center;
          justify-content: center;
          width: 2rem;
          height: 2rem;
          margin-bottom: 1rem;
          border: 0;
          font-size: 1.5rem;
          z-index: 999;
          &:hover {
            color: grey;
          }
          user-select: none;
        `}
        onClick={() => setIsMenuOpen(!isMenuOpen)}
      >
        {isMenuEnabledAndOpen ? `×` : `⁋`}
      </button>
      {isMenuEnabledAndOpen ? (
        <div
          className={css`
            position: relative;
            width: 400px;
            height: 100%;
            padding-top: 3rem;
            border-left: 1px dotted;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            overflow-x: hidden;
          `}
        >
          <div
            className={css`
              padding: 1rem;
              display: flex;
              justify-content: space-between;

              > * {
                display: flex;
                align-items: center;
              }
            `}
          >
            <Button onClick={saveExistingFile}>SAVE FILE</Button>
            <Button
              onClick={() => setSelectedPage(null)}
              disabled={selectedPage === null}
            >
              CLOSE PAGE
            </Button>
            <Button
              onClick={() => {
                setSelectedPage(null);
                setFileState(null);
              }}
              disabled={fileState === null}
            >
              CLOSE FILE
            </Button>
          </div>
          <VariablesEditor />
        </div>
      ) : null}
    </menu>
  );
}
