import React from 'react';
import { css } from '@emotion/css';
import { AppContext } from 'renderer/state';

export function Notifications() {
  const { editorStateDirty } = React.useContext(AppContext);
  return (
    <div
      className={css`
        position: fixed;
        z-index: 999;
        bottom: 0;
        right: 0;
      `}
    >
      {editorStateDirty ? (
        <div
          className={css`
            position: absolute;
            right: 0.4rem;
            bottom: 0.4rem;
            background-color: grey;
            height: 0.4rem;
            width: 0.4rem;
            border-radius: 100%;
          `}
        />
      ) : null}
    </div>
  );
}
