import React from 'react';
import { css } from '@emotion/css';
import { DndContext, DragEndEvent } from '@dnd-kit/core';
import { SortableContext } from '@dnd-kit/sortable';
import { Icon } from 'renderer/icon';
import { AppContext } from 'renderer/state';
import { SortableItem } from './SortableItem';
import { Button } from './Button';

function SortablePageItem({ id }: { id: string }) {
  const [sortingDisabled, setSortingDisabled] = React.useState(true);
  const { setSelectedPage, fileState } = React.useContext(AppContext);

  return (
    <SortableItem id={`${id}`} disabled={sortingDisabled}>
      <button
        type="button"
        key={id}
        className={css`
          position: relative;
          display: flex;
          width: 100%;
          aspect-ratio: 8.5/11;
          text-align: left;
          box-sizing: border-box;
          padding: 1rem;
          background-color: white;
          box-shadow: 2px 2px 10px -8px;
          &:hover,
          &:focus {
            outline: 2px solid grey;
            > .buttons {
              display: block;
            }
          }
          & + & {
            margin-left: 2%;
          }
          font-size: 1rem;
          overflow: hidden;
          &:after {
            content: ' ';
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(transparent, white);
          }
        `}
        onClick={() => setSelectedPage(id)}
      >
        {fileState?.pages[id]?.rawText.split('\n').map((value, index) => (
          // eslint-disable-next-line
          <React.Fragment key={index}>
            {value}
            <br />
          </React.Fragment>
        ))}
        <div
          className={
            'buttons ' +
            css`
              display: none;
              position: absolute;
              left: 0;
              top: 0;
              width: 100%;
              height: 100%;
              z-index: 1;
            `
          }
        >
          <Button
            onMouseEnter={() => setSortingDisabled(false)}
            onMouseLeave={() => setSortingDisabled(true)}
            className={css`
              position: absolute;
              right: 1rem;
              bottom: 1rem;
              cursor: move;
              width: 2rem;
              height: 2rem;
              display: flex;
              align-items: center;
              justify-content: center;
              > span {
                position: absolute;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
              }
            `}
            opaque
          >
            <Icon.Move />
          </Button>
        </div>
      </button>
    </SortableItem>
  );
}

export function PageSelector() {
  const { fileState, pageIdsInOrder, addNewPage, setPageState } =
    React.useContext(AppContext);

  if (fileState === null) return null;

  // TODO: repeated code (VariablesEditor)
  const handleDragEnd = (ev: DragEndEvent) => {
    const selectedIndex = ev.active.data.current?.sortable.index;
    const replacedIndex = ev.over?.data.current?.sortable.index;

    if (selectedIndex === undefined || replacedIndex === undefined) return;

    const pagesCopy = fileState.pages;
    pagesCopy[pageIdsInOrder[selectedIndex]].order = replacedIndex;

    if (selectedIndex < replacedIndex) {
      for (let i = selectedIndex + 1; i <= replacedIndex; i++) {
        const currentOrder = pagesCopy[pageIdsInOrder[i]].order;
        pagesCopy[pageIdsInOrder[i]].order = currentOrder - 1;
      }
    }

    if (selectedIndex > replacedIndex) {
      for (let i = replacedIndex; i < selectedIndex; i++) {
        const currentOrder = pagesCopy[pageIdsInOrder[i]].order;
        pagesCopy[pageIdsInOrder[i]].order = currentOrder + 1;
      }
    }

    setPageState(pagesCopy);
  };

  return (
    <DndContext onDragEnd={handleDragEnd}>
      <SortableContext items={pageIdsInOrder as string[]}>
        <div
          className={css`
            width: 100%;
            height: 100%;
            background-color: lightgray;
            padding: 2rem;
            overflow-y: auto;
          `}
        >
          <div
            className={css`
              margin-bottom: 2rem;
              display: flex;
              align-items: center;
              justify-content: center;
            `}
          >
            <span
              className={css`
                margin-right: 1rem;
              `}
            >
              SELECT/REORDER PAGES
            </span>
            <Button onClick={() => addNewPage()}>+ new page</Button>
          </div>
          <div
            className={css`
              display: grid;
              min-width: 100%;
              grid-template-columns: repeat(4, 1fr);
              grid-template-rows: auto;
              gap: 1rem;
            `}
          >
            {pageIdsInOrder.map((id) => (
              <SortablePageItem key={id} id={id} />
            ))}
          </div>
        </div>
      </SortableContext>
    </DndContext>
  );
}
