import React from 'react';
import { css } from '@emotion/css';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

export function SortableItem({
  id,
  disabled,
  children,
}: {
  id: string;
  disabled?: boolean;
  children: React.ReactNode;
}) {
  const {
    attributes,
    listeners,
    setNodeRef,
    transform,
    transition,
    isDragging,
  } = useSortable({ id, disabled });
  const style = {
    transform: CSS.Translate.toString(transform),
    transition,
  };

  return (
    <div
      ref={setNodeRef}
      style={style}
      className={css`
        ${isDragging ? 'z-index: 10;' : ''}
        &:hover {
          ${disabled ? '' : 'cursor: move;'}
        }
      `}
      {...attributes}
      {...listeners}
    >
      {children}
    </div>
  );
}
