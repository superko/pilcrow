import React from 'react';
import {
  DraftHandleValue,
  Editor,
  EditorState,
  getDefaultKeyBinding,
  Modifier,
  SelectionState,
} from 'draft-js';
import 'draft-js/dist/Draft.css';
import { css } from '@emotion/css';
import { AppContext } from 'renderer/state';
import { withCtrlOrMetaKey } from 'renderer/utils';
import { insertVariableIntoEditorState } from 'renderer/utils/variables';
import { VariableSelector } from './VariableSelector';

export function TextEditor() {
  const { editorState, setEditorState, saveExistingFile, fileState } =
    React.useContext(AppContext);
  const [variableSelectorVisible, setVariableSelectorVisible] =
    React.useState(false);
  const [savedSelection, setSavedSelection] =
    React.useState<null | SelectionState>(null);

  if (!editorState) return null;

  const handleKeyDown = (ev: React.KeyboardEvent<Record<string, never>>) => {
    if (withCtrlOrMetaKey(ev, 'KeyS')) {
      saveExistingFile();
    }
    if (withCtrlOrMetaKey(ev, 'Space')) {
      setSavedSelection(editorState.getSelection());
      setVariableSelectorVisible(true);
    }
    return getDefaultKeyBinding(ev);
  };

  const handleCloseVariableSelector = (forceFocus = true) => {
    setVariableSelectorVisible(false);
    if (forceFocus && savedSelection) {
      setEditorState(EditorState.forceSelection(editorState, savedSelection));
    }
  };

  const handleInsertVariable = (id: string) => {
    if (!savedSelection) return;
    const variables = fileState?.variables;
    if (!variables) return;

    const editorStateWithVariable = insertVariableIntoEditorState({
      editorState,
      selection: savedSelection,
      href: id,
    });

    setEditorState(editorStateWithVariable);
  };

  // HACK: fix multiline deletion bug
  const handleBeforeInput = (chars: string, edState: EditorState) => {
    const currentContentState = edState.getCurrentContent();
    const selectionState = edState.getSelection();
    setEditorState(
      EditorState.push(
        edState,
        Modifier.replaceText(currentContentState, selectionState, chars),
        'change-block-data'
      )
    );
    return 'handled' as DraftHandleValue;
  };

  return (
    <div
      className={css`
        width: 100%;
        min-width: 0;
        height: auto;
        max-height: 100%;
        overflow: hidden;
        font-size: 1.2rem;
        line-height: 1.5rem;
        display: flex;
        flex-direction: column;

        > *:first-child {
          width: 100%;
          height: 100%;
        }
        .public-DraftEditor-content {
          overflow-y: auto;
          padding: 2rem;
          &::-webkit-scrollbar {
            width: 0;
          }
        }
      `}
    >
      <Editor
        editorState={editorState}
        onChange={setEditorState}
        keyBindingFn={handleKeyDown}
        handleBeforeInput={handleBeforeInput}
      />
      <VariableSelector
        visible={variableSelectorVisible}
        onClose={handleCloseVariableSelector}
        onInsertVariable={handleInsertVariable}
      />
    </div>
  );
}
