import React, { useEffect } from 'react';
import { css } from '@emotion/css';
import { withCtrlOrMetaKey } from 'renderer/utils';
import { AppContext } from 'renderer/state';

interface Props {
  visible?: boolean;
  onClose: (forceFocus?: boolean) => void;
  onInsertVariable: (id: string) => void;
}

export function VariableSelector({
  visible,
  onClose,
  onInsertVariable,
}: Props) {
  const inputRef = React.useRef<HTMLInputElement>(null);
  const [inputValue, setInputValue] = React.useState('');
  const [isFocused, setIsFocused] = React.useState(false);
  const [cursor, setCursor] = React.useState(-1);

  const { fileState, variableIdsInOrder, addNewVariable } =
    React.useContext(AppContext);

  const filteredVariables = variableIdsInOrder.filter((id) => {
    return inputValue
      ? fileState?.variables[id].label.startsWith(inputValue)
      : true;
  });

  useEffect(() => {
    inputRef?.current?.focus();
  }, [inputRef, visible]);

  const handleChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(ev.target.value);
  };

  const handleClose = (forceFocus = true) => {
    setCursor(-1);
    setInputValue('');
    onClose(forceFocus);
  };

  const handleInsertVariable = (id: string) => {
    onInsertVariable(id);
    handleClose(false);
  };

  const handleKeyDown = (ev: React.KeyboardEvent<HTMLInputElement>) => {
    if (ev.code === 'ArrowUp') {
      if (cursor <= 0) {
        setCursor(filteredVariables.length - 1);
      } else {
        setCursor(cursor - 1);
      }
    }
    if (ev.code === 'ArrowDown') {
      if (cursor === filteredVariables.length - 1) {
        setCursor(0);
      } else {
        setCursor(cursor + 1);
      }
    }

    if (ev.code === 'Enter') {
      ev.preventDefault();
      const id = filteredVariables[cursor];
      if (!id) return;
      handleInsertVariable(id);
    }
    if (ev.code === 'Escape' || withCtrlOrMetaKey(ev, 'Space')) {
      handleClose();
    }
  };

  const createNewVariable = () => {
    addNewVariable(inputValue);
    handleClose(false);
  };

  if (!visible) return null;
  return (
    <div
      className={css`
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        margin: auto;
        width: 300px;
        z-index: 999;
        display: flex;
        align-items: center;
        justify-content: center;
      `}
    >
      <input
        ref={inputRef}
        list="variablesData"
        id="variablesInput"
        name="variablesInput"
        className={css`
          width: 100%;
          border: 1px solid;
          border-top: 0;
          outline: 0;
          padding: 0.5rem 1rem;
          margin: 0;
        `}
        onKeyDown={handleKeyDown}
        onChange={handleChange}
        value={inputValue}
        onFocus={() => setIsFocused(true)}
        onBlur={() => setIsFocused(false)}
      />
      <button
        type="button"
        className={css`
          position: absolute;
          left: 101%;
          height: 100%;
          padding: 0.5rem 1rem;
          background-color: black;
          color: white;
          &:hover,
          &:focus {
            background-color: grey;
          }
          &:disabled {
            text-decoration: line-through;
            background-color: lightgray;
          }
        `}
        onClick={createNewVariable}
        disabled={!inputValue}
      >
        NEW
      </button>
      <ul
        className={css`
          position: absolute;
          top: 100%;
          left: 0;
          width: 100%;
          border: 1px solid grey;
          border-top: 0;
          max-height: 50vh;
          overflow-y: auto;
          margin: 0;
          padding: 0;
          list-style: none;
        `}
      >
        {filteredVariables.map((id, index) => (
          <li key={id}>
            <button
              type="button"
              className={css`
                padding: 0.2rem 0.5rem;
                user-select: none;
                width: 100%;
                text-align: left;
                ${cursor === index
                  ? 'background-color: lightgrey;'
                  : `
                &:hover, &:focus {
                  background-color: lightgrey;
                }
                `}
              `}
              onClick={() => handleInsertVariable(id)}
            >
              {fileState?.variables[id].label}
            </button>
          </li>
        ))}
      </ul>

      {isFocused ? null : (
        <button
          type="button"
          className={css`
            position: absolute;
            right: 101%;
          `}
          onClick={() => handleClose(true)}
          title="close"
        >
          ×
        </button>
      )}
      <label
        htmlFor="variablesInput"
        className={css`
          display: none;
          position: absolute;
          top: 100%;
          left: 0;
          width: 100%;
          background-color: black;
          color: white;
          font-size: 80%;
          text-transform: uppercase;
          text-align: center;
        `}
      >
        Variables
      </label>
    </div>
  );
}
