import React from 'react';
import { ContentState, Editor, EditorState } from 'draft-js';
import { DndContext, DragEndEvent } from '@dnd-kit/core';
import {
  SortableContext,
  verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import { css } from '@emotion/css';
import { AppContext } from 'renderer/state';
import { Icon } from 'renderer/icon';
import { SortableItem } from './SortableItem';
import { Button } from './Button';

const inputHeightValue = '2rem';
const inputHeight = `height: ${inputHeightValue};`;

function SortableVariableItem({
  id,
  disabled: sortingDisabled,
  descriptionVisible,
}: {
  id: string;
  disabled?: boolean;
  descriptionVisible?: boolean;
}) {
  const { fileState, setVariableName, setVariableDescription } =
    React.useContext(AppContext);

  const item = fileState?.variables[id];

  const [labelValue, setLabelValue] = React.useState(item?.label || '');
  const [descriptionState, setDescriptionState] = React.useState(() =>
    EditorState.createWithContent(
      ContentState.createFromText(item?.description || '')
    )
  );

  if (!fileState) return null;

  return (
    <SortableItem id={id} disabled={sortingDisabled}>
      <div
        className={css`
          margin: 0.5rem 0;
          display: flex;
        `}
        tabIndex={-1}
      >
        <div
          className={css`
            display: flex;
            flex-direction: column;
            width: 100%;
          `}
        >
          <input
            type="text"
            onBlur={() => setVariableName(id, labelValue)}
            onChange={(ev) => setLabelValue(ev.target.value)}
            value={labelValue}
            disabled={!sortingDisabled}
            className={css`
              ${inputHeight}
            `}
          />
          {descriptionVisible ? (
            <div
              className={css`
                min-height: ${inputHeightValue};
                max-width: 100%;
                flex-shrink: 1;
                padding: 0.5rem;
                border: 1px dotted;
                border-top: 0;
                ${!sortingDisabled ? 'opacity: 0.7;' : ''}
              `}
            >
              <Editor
                editorState={descriptionState}
                onChange={setDescriptionState}
                onBlur={() =>
                  setVariableDescription(
                    id,
                    descriptionState.getCurrentContent().getPlainText()
                  )
                }
              />
            </div>
          ) : null}
        </div>

        {sortingDisabled ? null : (
          <div
            className={css`
              margin-left: 0.5rem;
              border: 1px solid;
              font-size: 1.2rem;
              user-select: none;
              padding: 0 0.5rem;
              display: flex;
              align-items: center;
              ${inputHeight}
            `}
          >
            <Icon.Move />
          </div>
        )}
      </div>
    </SortableItem>
  );
}

export function VariablesEditor() {
  const { variableIdsInOrder, fileState, setVariableState } =
    React.useContext(AppContext);
  const [editingNames, setEditingNames] = React.useState(false);
  const [descriptionsVisible, setDescriptionsVisible] = React.useState(false);

  if (fileState === null) return null;

  // TODO: repeated code (PageSelector)
  const handleDragEnd = (ev: DragEndEvent) => {
    const selectedIndex = ev.active.data.current?.sortable.index;
    const replacedIndex = ev.over?.data.current?.sortable.index;

    if (selectedIndex === undefined || replacedIndex === undefined) return;

    const variablesCopy = fileState.variables;
    variablesCopy[variableIdsInOrder[selectedIndex]].order = replacedIndex;

    if (selectedIndex < replacedIndex) {
      for (let i = selectedIndex + 1; i <= replacedIndex; i++) {
        const currentOrder = variablesCopy[variableIdsInOrder[i]].order;
        variablesCopy[variableIdsInOrder[i]].order = currentOrder - 1;
      }
    }

    if (selectedIndex > replacedIndex) {
      for (let i = replacedIndex; i < selectedIndex; i++) {
        const currentOrder = variablesCopy[variableIdsInOrder[i]].order;
        variablesCopy[variableIdsInOrder[i]].order = currentOrder + 1;
      }
    }

    setVariableState(variablesCopy);
  };

  return (
    <DndContext onDragEnd={handleDragEnd}>
      <SortableContext
        items={variableIdsInOrder}
        strategy={verticalListSortingStrategy}
      >
        <div
          className={css`
            height: 100%;
            width: 100%;
            overflow-x: hidden;
            overflow-y: auto;
            &::-webkit-scrollbar {
              width: 0;
            }
          `}
        >
          <div
            className={css`
              position: sticky;
              top: 0;
              padding: 1.5rem 1rem;
              border-top: 1px dotted;
              background-color: white;
              display: flex;
              align-items: center;
              justify-content: space-between;
              z-index: 100;
            `}
          >
            <span>VARIABLES</span>
            <div>
              <Button
                onClick={() => setEditingNames(!editingNames)}
                title="Toggle editing mode"
              >
                {editingNames ? '🖉 ORDER' : '🖉 TEXTS'}
              </Button>
              <Button
                onClick={() => setDescriptionsVisible(!descriptionsVisible)}
                title="Toggle descriptions"
              >
                ⋮
              </Button>
            </div>
          </div>
          <div
            className={css`
              padding: 1rem;
              padding-top: 0;
            `}
          >
            {variableIdsInOrder.map((id) => {
              const variables = fileState?.variables;
              if (!variables) return null;
              const v = variables[id];
              if (!v) return null;
              return (
                <SortableVariableItem
                  key={id}
                  id={id}
                  disabled={editingNames}
                  descriptionVisible={descriptionsVisible}
                />
              );
            })}
          </div>
        </div>
      </SortableContext>
    </DndContext>
  );
}
