import { Move } from './Move';
import { Trash } from './Trash';

export const Icon = {
  Move,
  Trash,
};
