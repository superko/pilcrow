import type fs from 'fs';
import type { FileState } from './state/types';

export interface ElectronAPI {
  showSaveDialog: (args: Electron.SaveDialogOptions) => string | undefined;
  showOpenDialog: (args: Electron.OpenDialogOptions) => string | undefined;
  writeFile: (args: {
    file: fs.PathOrFileDescriptor;
    data: string | NodeJS.ArrayBufferView;
  }) => void;
  loadFile: (filepath: string) => FileState;
}

declare global {
  interface Window {
    electronAPI: ElectronAPI;
  }
}
