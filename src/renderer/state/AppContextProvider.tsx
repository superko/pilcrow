import React from 'react';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';
import { saveFile } from 'renderer/utils';
import { decorator } from 'renderer/utils/variables';
import { AppContext } from '.';
import type { FileState, Page, Variable } from './types';
import { createBlankPage, stateToSaveData } from './utils';

export function AppContextProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [fileState, setFileState] = React.useState<FileState | null>(null);
  const [selectedFilePath, setSelectedFilePath] = React.useState<string | null>(
    null
  );
  const [selectedPage, setSelectedPage] = React.useState<string | null>(null);
  const [editorState, setEditorState] = React.useState<EditorState | null>(
    null
  );

  React.useEffect(() => {
    if (selectedPage !== null && fileState !== null) {
      setEditorState(
        EditorState.createWithContent(
          convertFromRaw(fileState.pages[selectedPage].editorContentState),
          decorator
        )
      );
    }
  }, [selectedPage]);

  const frozenFileState = React.useMemo(() => {
    if (fileState === null || selectedPage === null) return null;
    return JSON.stringify(fileState?.pages[selectedPage].editorContentState);
  }, [fileState, selectedPage]);

  const editorStateDirty = React.useMemo(() => {
    if (editorState === null) return null;
    return (
      frozenFileState !==
      JSON.stringify(convertToRaw(editorState.getCurrentContent()))
    );
  }, [frozenFileState, editorState]);

  const saveExistingFile = () => {
    if (
      selectedPage === null ||
      selectedFilePath === null ||
      fileState === null ||
      editorState === null
    )
      return;

    const data = stateToSaveData({
      selectedPage,
      fileState,
      editorState,
    });

    if (data === null) return;

    setFileState((prevState) => {
      if (prevState === null) return null;
      return {
        ...prevState,
        pages: {
          ...prevState.pages,
          [selectedPage]: {
            ...prevState.pages[selectedPage],
            editorContentState: convertToRaw(editorState?.getCurrentContent()),
          },
        },
      };
    });

    saveFile({
      filepath: selectedFilePath,
      data,
    });
  };

  const variablesString = React.useMemo(() => {
    if (fileState === null) return '';
    return JSON.stringify(fileState.variables);
  }, [fileState]);

  const variablesReverseLookup = React.useMemo(() => {
    if (fileState === null) return {};
    const vars: {
      [key: string]: string;
    } = {};

    Object.keys(fileState.variables).forEach((id) => {
      const v = fileState.variables[id];
      vars[v.label] = id;
    });

    return vars;
  }, [variablesString]);

  const variableIdsInOrder = React.useMemo(() => {
    if (fileState === null) return [];
    return Object.keys(fileState.variables).sort(
      (a, b) => fileState.variables[a].order - fileState.variables[b].order
    );
  }, [variablesString]);

  const addNewVariable = (label: string, description = '') => {
    setFileState((prevState) => {
      if (prevState === null) return null;
      const id = Date.now();
      return {
        ...prevState,
        variables: {
          ...prevState.variables,
          [id]: {
            label,
            description,
            order: variableIdsInOrder.length,
          },
        },
      };
    });
  };

  const setVariableState = (v: { [key: string]: Variable }) => {
    setFileState((prevState) => {
      if (prevState === null) return null;
      return {
        ...prevState,
        variables: {
          ...prevState.variables,
          ...v,
        },
      };
    });
  };

  const setVariableName = (id: string, label: string) => {
    setFileState((prevState) => {
      if (prevState === null) return null;
      if (!prevState.variables[id]) return prevState;
      return {
        ...prevState,
        variables: {
          ...prevState.variables,
          [id]: {
            ...prevState.variables[id],
            label,
          },
        },
      };
    });
  };

  const setVariableDescription = (id: string, description: string) => {
    setFileState((prevState) => {
      if (prevState === null) return null;
      if (!prevState.variables[id]) return prevState;
      return {
        ...prevState,
        variables: {
          ...prevState.variables,
          [id]: {
            ...prevState.variables[id],
            description,
          },
        },
      };
    });
  };

  const pagesString = React.useMemo(() => {
    if (fileState === null) return '';
    return JSON.stringify(fileState.pages);
  }, [fileState]);

  const pageIdsInOrder = React.useMemo(() => {
    if (fileState === null) return [];
    return Object.keys(fileState.pages)
      .sort((a, b) => fileState.pages[a].order - fileState.pages[b].order)
      .map((pageId) => pageId);
  }, [pagesString]);

  const addNewPage = () => {
    const newPage = createBlankPage(pageIdsInOrder.length);
    setFileState((prevState) => {
      if (prevState === null) return null;
      return {
        ...prevState,
        pages: {
          ...prevState.pages,
          ...newPage,
        },
      };
    });
  };

  const setPageState = (p: { [key: string]: Page }) => {
    setFileState((prevState) => {
      if (prevState === null) return null;
      return {
        ...prevState,
        pages: {
          ...prevState.pages,
          ...p,
        },
      };
    });
  };

  return (
    <AppContext.Provider
      value={{
        fileState,
        setFileState,
        pageIdsInOrder,
        saveExistingFile,
        selectedFilePath,
        setSelectedFilePath,
        selectedPage,
        setSelectedPage,
        editorState,
        setEditorState,
        editorStateDirty,
        addNewVariable,
        setVariableState,
        setVariableName,
        setVariableDescription,
        addNewPage,
        setPageState,
        variableIdsInOrder,
        variablesReverseLookup,
      }}
    >
      {children}
    </AppContext.Provider>
  );
}
