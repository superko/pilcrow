import type { EditorState, RawDraftContentState } from 'draft-js';

export interface Page {
  order: number;
  editorContentState: RawDraftContentState;
  rawText: string;
}

export interface Variable {
  order: number;
  label: string;
  description: string;
}

export interface FileState {
  pages: {
    [id: string]: Page;
  };
  variables: {
    [id: string]: Variable;
  };
}

export interface AppContextType {
  fileState: FileState | null;
  setFileState: (s: FileState | null) => void;
  pageIdsInOrder: string[];
  saveExistingFile: () => void;
  selectedFilePath: string | null;
  setSelectedFilePath: (s: string) => void;
  selectedPage: string | null;
  setSelectedPage: (page: string | null) => void;
  editorState: EditorState | null;
  setEditorState: (s: EditorState) => void;
  editorStateDirty: boolean | null;
  addNewVariable: (label: string, description?: string) => void;
  addNewPage: () => void;
  setPageState: (p: { [id: string]: Page }) => void;
  variableIdsInOrder: string[];
  variablesReverseLookup: {
    [variable: string]: string;
  };
  setVariableState: (v: { [id: string]: Variable }) => void;
  setVariableName: (id: string, label: string) => void;
  setVariableDescription: (id: string, label: string) => void;
}
