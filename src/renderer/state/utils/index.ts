import { ContentState, EditorState, convertToRaw } from 'draft-js';
import { FileState, Page } from '../types';

const blankContentState = ContentState.createFromText('');
const blankContentStateRaw = convertToRaw(blankContentState);

export const blankEditorState = EditorState.createEmpty();

export const blankFileState: FileState = {
  pages: {
    0: {
      order: 0,
      editorContentState: blankContentStateRaw,
      rawText: '',
    },
  },
  variables: {},
};

export const createBlankPage = (
  order: number
): {
  [key: string]: Page;
} => {
  const id = Date.now();
  return {
    [id]: {
      order,
      editorContentState: blankContentStateRaw,
      rawText: '',
    },
  };
};

export const stateToSaveData = ({
  selectedPage,
  fileState,
  editorState,
}: {
  selectedPage: string;
  fileState: FileState;
  editorState: EditorState;
}) => {
  const fileStateCopy = { ...fileState };
  if (!fileStateCopy.pages[selectedPage]) return null;
  const contentState = editorState.getCurrentContent();

  fileStateCopy.pages[selectedPage].editorContentState =
    convertToRaw(contentState);
  fileStateCopy.pages[selectedPage].rawText = contentState.getPlainText();

  return JSON.stringify(fileStateCopy);
};
