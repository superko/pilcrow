import {
  blankEditorState,
  blankFileState,
  stateToSaveData,
} from 'renderer/state/utils';
import { dialogFilters } from './dialogFilters';
import { saveFile } from './saveFile';

export function createNewFile(name = 'my_project') {
  const filepath = window.electronAPI.showSaveDialog({
    defaultPath: `${name}.pilcrow`,
    filters: dialogFilters,
    properties: ['createDirectory', 'showOverwriteConfirmation'],
  });

  if (!filepath) return false;

  const data = stateToSaveData({
    selectedPage: 0,
    fileState: blankFileState,
    editorState: blankEditorState,
  });

  if (data === null) return false;

  saveFile({
    filepath,
    data,
  });

  return filepath;
}
