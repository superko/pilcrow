export const dialogFilters = [
  {
    name: 'Pilcrow files',
    extensions: ['pilcrow'],
  },
  {
    name: 'All files',
    extensions: ['*'],
  },
];
