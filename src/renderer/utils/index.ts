export function readFile(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = (ev) => {
      const text = ev.target?.result as string;
      const json = JSON.parse(text);
      resolve(json);
    };
    reader.onerror = (ev) => {
      reject(ev);
    };
    reader.readAsText(file);
  });
}

export * from './createNewFile';
export * from './saveFile';
export * from './loadFile';
export * from './withCtrlOrMetaKey';
