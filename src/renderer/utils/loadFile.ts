import { dialogFilters } from './dialogFilters';

export function showOpenDialog() {
  const filepath = window.electronAPI.showOpenDialog({
    filters: dialogFilters,
  });
  return filepath;
}

export function loadFile(filepath: string) {
  const file = window.electronAPI.loadFile(filepath);
  return file;
}
