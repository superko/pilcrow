export function saveFile({
  filepath,
  data,
}: {
  filepath: string;
  data: string;
}) {
  window.electronAPI.writeFile({
    file: filepath,
    data,
  });
}
