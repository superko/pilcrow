import React from 'react';
import {
  CharacterMetadata,
  CompositeDecorator,
  ContentBlock,
  ContentState,
  convertFromHTML,
  EditorState,
  Modifier,
  SelectionState,
} from 'draft-js';
import { css } from '@emotion/css';
import { AppContext } from 'renderer/state';

interface Props {
  contentState: ContentState;
  entityKey: string;
  children: any;
}

export function Variable({ contentState, entityKey, children }: Props) {
  const { fileState } = React.useContext(AppContext);
  const { href } = contentState.getEntity(entityKey).getData();
  return (
    <a
      href={`#${href}`}
      className={css`
        border: 1px solid lightgrey;
        background-color: #eee;
        border-radius: 0.1rem;
        font-size: 90%;
        text-decoration: none;
        color: black;
      `}
      contentEditable={false}
    >
      {fileState?.variables?.[href]?.label || null}
      <div
        className={css`
          display: none;
        `}
      >
        {children}
      </div>
    </a>
  );
}

export const findLinkEntities = (
  contentBlock: ContentBlock,
  callback: (start: number, end: number) => void,
  contentState: ContentState
) => {
  contentBlock.findEntityRanges((character: CharacterMetadata) => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === 'LINK'
    );
  }, callback);
};

export const decorator = new CompositeDecorator([
  { strategy: findLinkEntities, component: Variable },
]);

export const insertVariableIntoEditorState = ({
  editorState,
  selection,
  href,
}: {
  editorState: EditorState;
  selection: SelectionState;
  href: string;
}) => {
  const markup = `<a href="${href}">%</a>`;
  const { contentBlocks, entityMap } = convertFromHTML(markup);

  const contentStateWithVariable = Modifier.replaceWithFragment(
    editorState.getCurrentContent(),
    selection,
    ContentState.createFromBlockArray(contentBlocks, entityMap).getBlockMap()
  );
  const editorStateWithVariable = EditorState.push(
    editorState,
    contentStateWithVariable,
    'change-block-data'
  );

  // HACK: force add a space to prevent the stuck decorator-selection glitch
  const newContentState = editorStateWithVariable.getCurrentContent();
  const newSelectionState = newContentState.getSelectionAfter();
  const lastContentState = Modifier.insertText(
    newContentState,
    newSelectionState,
    ' '
  );
  const lastEditorState = EditorState.push(
    editorStateWithVariable,
    lastContentState,
    'insert-characters'
  );

  return lastEditorState;
};
