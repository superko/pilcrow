import React from 'react';

export function withCtrlOrMetaKey(
  ev: React.KeyboardEvent<unknown>,
  code?: string
) {
  const isCtrlOrMeta = ev.ctrlKey || ev.metaKey;
  return code ? isCtrlOrMeta && ev.code === code : isCtrlOrMeta;
}
